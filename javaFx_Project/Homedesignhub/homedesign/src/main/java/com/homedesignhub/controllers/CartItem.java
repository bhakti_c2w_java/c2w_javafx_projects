package com.homedesignhub.controllers;

public class CartItem {
    private String imageUrl;
    private String price;
    private String itemType; // "Template" or "Furniture"

    public CartItem(String imageUrl, String price, String itemType) {
        this.imageUrl = imageUrl;
        this.price = price;
        this.itemType = itemType;
    }

   

    public CartItem(String imageUrl2, String price2, String rating, String furniture, String dimension, String style) {
        //TODO Auto-generated constructor stub
    }



    public String getImageUrl() {
        return imageUrl;
    }

    public String getPrice() {
        return price;
    }

    public String getItemType() {
        return itemType != null ? itemType : "Unknown";
    }
}
