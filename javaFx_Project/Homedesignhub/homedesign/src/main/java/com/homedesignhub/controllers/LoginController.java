 package com.homedesignhub.controllers;
 
 import javafx.animation.FadeTransition;
 import javafx.geometry.Insets;
 import javafx.geometry.Pos;
 import javafx.scene.Group;
 import javafx.scene.Scene;
 import javafx.scene.control.Alert;
 import javafx.scene.control.Button;
 import javafx.scene.control.Label;
 import javafx.scene.control.PasswordField;
 import javafx.scene.control.TextField;
 import javafx.scene.image.Image;
 import javafx.scene.image.ImageView;
 import javafx.scene.layout.HBox;
 import javafx.scene.layout.Pane;
 import javafx.scene.layout.StackPane;
 import javafx.scene.layout.VBox;
 import javafx.scene.paint.Color;
 import javafx.scene.shape.Rectangle;
 import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
 import javafx.util.Duration;
 
 import java.io.IOException;
 import java.util.concurrent.ExecutionException;
 import com.homedesignhub.Sevices.firebaseconfig;;
 
 public class LoginController {
 
     private Stage primaryStage;
     private Scene loginScene;
     private Scene roomCategorySelectionScene;
     private SignupController signupController;
     private HomeRenovationApp homeRenovationApp;
     private firebaseconfig firebase;
 
     public LoginController(Stage primaryStage, HomeRenovationApp homeRenovationApp) {
         this.primaryStage = primaryStage;
         this.homeRenovationApp = homeRenovationApp;
         firebase = new firebaseconfig();
         initScenes();
     }
 
     private void initScenes() {
         initLoginScene();
         initRoomCategorySelectionScene();
     }
 
     public void initLoginScene() {
           Label messageLabel = new Label("Welcome to Home Design Hub");
        messageLabel.setFont(Font.font("Verdana", FontWeight.EXTRA_BOLD, 40));
        messageLabel.setTextFill(Color.BLACK);
        messageLabel.setAlignment(Pos.CENTER);

        Rectangle messageBackground = new Rectangle();
        messageBackground.setFill(Color.WHITE);
        messageBackground.setStroke(Color.BLACK);
        messageBackground.setArcWidth(30);
        messageBackground.setArcHeight(30);
        messageBackground.setWidth(700); // Ensure the rectangle width is consistent
        messageBackground.setHeight(100); // Ensure the rectangle height is consistent

        StackPane messageBox = new StackPane();
        messageBox.getChildren().addAll(messageBackground, messageLabel);
        messageBox.setAlignment(Pos.CENTER);
        messageBox.setPadding(new Insets(40));
        messageBox.setStyle("-fx-background-color: rgba(255,255,255,0.5); -fx-background-radius: 30; -fx-border-radius: 30;");

        messageBox.setMaxWidth(700);

        VBox vBoxMessage = new VBox(messageBox);
        vBoxMessage.setAlignment(Pos.CENTER);

        // Create a pane for the background images
        Pane backgroundPane = new Pane();
        backgroundPane.setPrefSize(1920, 1000);

        // Initialize two ImageView objects
        ImageView backgroundImageView1 = new ImageView();
        ImageView backgroundImageView2 = new ImageView();

        backgroundImageView1.setFitWidth(1920);
        backgroundImageView1.setFitHeight(1000);
        backgroundImageView1.setPreserveRatio(false);

        backgroundImageView2.setFitWidth(1920);
        backgroundImageView2.setFitHeight(1000);
        backgroundImageView2.setPreserveRatio(false);

        backgroundPane.getChildren().addAll(backgroundImageView1, backgroundImageView2);
     
 
         Image logoImage = new Image("https://i.pinimg.com/564x/15/0c/f0/150cf031e69a8e7b7697801a432dab6f.jpg");
         ImageView logoImageView = new ImageView(logoImage);
         logoImageView.setFitWidth(110);
         logoImageView.setFitHeight(50);
         logoImageView.setMouseTransparent(false);
 
         Image userIcon = new Image(getClass().getResourceAsStream("/images/username.jpg"));
         ImageView userIconView = new ImageView(userIcon);
         userIconView.setFitWidth(30);
         userIconView.setFitHeight(30);
         Label userLabel = new Label("Username", userIconView);
         userLabel.setStyle("-fx-text-fill:White;");
         userLabel.setFont(new Font(30));
         TextField userTextField = new TextField();
         userTextField.setPromptText("Enter Username");
         userTextField.setPrefSize(300, 70);
         userTextField.setStyle("-fx-background-color:White;-fx-text-fill:black;");
 
         Image passIcon = new Image(getClass().getResourceAsStream("/images/password.png"));
         ImageView passIconView = new ImageView(passIcon);
         passIconView.setFitWidth(30);
         passIconView.setFitHeight(30);
         Label passLabel = new Label("Password", passIconView);
         passLabel.setStyle("-fx-text-fill:White;");
         passLabel.setFont(new Font(30));
         PasswordField passField = new PasswordField();
         passField.setPromptText("Enter Password");
         passField.setStyle("-fx-background-color:White;-fx-text-fill:black;");
 
         Button loginButton = new Button("Login");
         loginButton.setFont(new Font(20));
         loginButton.setPrefSize(200, 40);
         loginButton.setStyle("-fx-text-fill:Black;-fx-background-color:white;-fx-background-radius: 10;");
         loginButton.setAlignment(Pos.TOP_CENTER);
         loginButton.setOnAction(event -> {
             try {
                 handleLogin(userTextField.getText(), passField.getText());
                 userTextField.clear();
                 passField.clear();
             } catch (ExecutionException | InterruptedException | IOException e) {
                 e.printStackTrace();
             }
         });
 
         VBox fieldBox1 = new VBox(10, userLabel, userTextField);
         fieldBox1.setMaxSize(500, 30);
 
         VBox fieldBox2 = new VBox(10, passLabel, passField);
         fieldBox2.setMaxSize(500, 40);
 
         HBox buttonBox = new HBox(loginButton);
         buttonBox.setMaxSize(500, 30);
         buttonBox.setAlignment(Pos.CENTER);
 
         VBox inputBox = new VBox(20, fieldBox1, fieldBox2, buttonBox);
         inputBox.setAlignment(Pos.CENTER);
 
         Rectangle rectangle = new Rectangle(700, 400);
         rectangle.setFill(Color.rgb(0, 0, 0, 0.5));
         rectangle.setArcWidth(20);
         rectangle.setArcHeight(20);
 
         StackPane stackPane = new StackPane(rectangle, inputBox);
         stackPane.setAlignment(Pos.CENTER);
 
         Image image = new Image(getClass().getResourceAsStream("/images/logo.jpg"));
         ImageView imageView = new ImageView(image);
         imageView.setFitWidth(1920);
         imageView.setFitHeight(1000);
 
         Pane iPane = new Pane();
         iPane.getChildren().addAll(imageView);
 
         Button loginPageNavigator = new Button("Login");
         loginPageNavigator.setTextFill(Color.TEAL);
         loginPageNavigator.setPrefSize(200, 30);
         loginPageNavigator.setFont(new Font(30));
         loginPageNavigator.setOnAction(event -> showLoginScene());
 
         Button signupPageNavigator = new Button("Sign Up");
         signupPageNavigator.setPrefSize(200, 50);
         signupPageNavigator.setFont(new Font(30));
         signupPageNavigator.setOnAction(event -> showSignupScene());
 
         HBox navigationButtonBox = new HBox(160, loginPageNavigator, signupPageNavigator);
         navigationButtonBox.setAlignment(Pos.CENTER);
         navigationButtonBox.setPrefSize(1700, 100);
 
         VBox vBoxMain = new VBox(40, vBoxMessage, stackPane, navigationButtonBox);
         vBoxMain.setPrefSize(1700, 900);
         vBoxMain.setAlignment(Pos.TOP_CENTER);
         vBoxMain.setLayoutY(140);
 
         Group gr = new Group(iPane, vBoxMain);
 
         loginScene = new Scene(gr, 1920, 1000);
         loginScene.setFill(Color.BLACK);
 
         applyFadeTransition(vBoxMain);
     }
 
     private void initRoomCategorySelectionScene() {
         RoomCategorySelection roomCategorySelection = new RoomCategorySelection(primaryStage);
         roomCategorySelectionScene = new Scene(roomCategorySelection, 1920, 1000);
     }
 
     public Scene getLoginScene() {
         if (loginScene == null) {
             initLoginScene();
         }
         return loginScene;
     }
 
     public void showLoginScene() {
         primaryStage.setScene(loginScene);
         primaryStage.setTitle("Login Page");
         primaryStage.show();
     }
 
     private void handleLogin(String username, String password) throws ExecutionException, InterruptedException, IOException {
         if (username.isEmpty() || password.isEmpty()) {
             showAlert("Invalid Login", "Please enter both username and password.");
             return;
         }
 
         // Call authentication method from firebaseconfig class
         boolean authenticated = firebase.authenticateUser(username, password);
 
         if (authenticated) {
             initRoomCategorySelectionScene();
             primaryStage.setScene(roomCategorySelectionScene);
         } else {
             showAlert("Invalid Login", "Invalid username or password.");
         }
     }
 
     private void showAlert(String title, String message) {
         Alert alert = new Alert(Alert.AlertType.ERROR);
         alert.setTitle(title);
         alert.setHeaderText(null);
         alert.setContentText(message);
         alert.showAndWait();
     }
 
     public void showSignupScene() {
         signupController = new SignupController(this);
         Scene signupScene = signupController.createSignupScene(primaryStage);
 
         primaryStage.setScene(signupScene);
         primaryStage.setTitle("Signup Page");
         primaryStage.show();
     }
 
     private void handleLogout() {
         primaryStage.setScene(loginScene);
         primaryStage.setTitle("Login Page");
     }
 
     private void applyFadeTransition(VBox vbox) {
         FadeTransition fadeTransition = new FadeTransition(Duration.millis(2000), vbox);
         fadeTransition.setFromValue(0.0);
         fadeTransition.setToValue(1.0);
         fadeTransition.play();
     }
 }
 