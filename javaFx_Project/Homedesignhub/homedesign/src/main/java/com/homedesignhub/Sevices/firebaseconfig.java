package com.homedesignhub.Sevices;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

import javafx.scene.control.Alert;

public class firebaseconfig {

    private static Firestore db;

    static {
        try {
            initializeFirebase();
            System.out.println("FireStore Initialized");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void initializeFirebase() throws IOException {
        FileInputStream serviceAccount = new FileInputStream("C:\\Users\\ADMIN\\Desktop\\zip\\Homedesignhub\\homedesign\\src\\main\\resources\\java-fx.json");

        @SuppressWarnings("deprecation")
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .build();

        FirebaseApp.initializeApp(options);
        db = FirestoreClient.getFirestore();
    }
    
    public void addData(String collection, String document, Map<String, Object> data) throws ExecutionException,InterruptedException {

            DocumentReference docRef = db.collection(collection).document(document);
            ApiFuture<WriteResult> result = docRef.set(data);
            result.get();
    }

    public DocumentSnapshot getData(String collection, String document) throws ExecutionException, InterruptedException {

        try { 
            DocumentReference docRef = db.collection(collection).document(document);
            ApiFuture<DocumentSnapshot> future = docRef.get();
            return future.get();
        } catch (Exception e) {

            e.printStackTrace();   
            throw e;
        }
    }



    public boolean authenticateUser(String username, String password) throws ExecutionException, InterruptedException {
        
        DocumentSnapshot document = db.collection("users").document(username).get().get();
    
        if (document.exists()) {
            String storedPassword = document.getString("password");
            return password.equals(storedPassword);
        
        }
    
        showAlert("Invalid Login","Invalid Credential....!");
        return false;
    }

    public void showAlert(String tital, String message) {
        
        Alert alert  = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(tital);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();

    }


    



    public List<Map<String, Object>> getProducts(String categoryName) throws ExecutionException, InterruptedException {
        List<Map<String, Object>> products = new ArrayList<>();
        System.out.println("Fetching products for category: " + categoryName);
        ApiFuture<QuerySnapshot> query = db.collection(categoryName).get();
        QuerySnapshot querySnapshot;
        try {
            querySnapshot = query.get();
            System.out.println("Documents found: " + querySnapshot.size());
            for (QueryDocumentSnapshot document : querySnapshot) {
                Map<String, Object> product = document.getData();
               // System.out.println("Fetched product data: " + product); // Debug print
                products.add(product);
            }
        } catch (InterruptedException | ExecutionException e) {
            System.err.println("Error fetching products from Firestore: " + e.getMessage());
            throw e;
        }
        return products;
    }
}
