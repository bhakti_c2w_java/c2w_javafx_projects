package com.homedesignhub.controllers;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;

public class CartPage extends VBox {

    private List<CartItem> cartItems;
    private Label totalPriceLabel;

    public CartPage(List<CartItem> cartItems) {
        this.cartItems = cartItems;

        // Set VBox properties
        setPadding(new Insets(20));
        setSpacing(20);
        setAlignment(Pos.TOP_CENTER);

        // Add a back button to navigate back to the room category page
        Button backButton = new Button("Home");
        backButton.setOnAction(e -> HomeRenovationApp.navigateToRoomCategoryPage());
        getChildren().add(backButton);

        // Initialize ScrollPane for cart items
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setPadding(new Insets(20));
        scrollPane.setFitToWidth(true); // Allow horizontal scrolling if needed
        scrollPane.setFitToHeight(true); // Allow vertical scrolling if needed

        // Initialize TilePane for cart items inside ScrollPane
        TilePane tilePane = new TilePane();
        tilePane.setPadding(new Insets(500));
        tilePane.setHgap(40);
        tilePane.setVgap(40);
        tilePane.setAlignment(Pos.CENTER);
        tilePane.setStyle("-fx-background-image: url('https://img.freepik.com/free-photo/abstract-luxury-gradient-blue-background-smooth-dark-blue-with-black-vignette-studio-banner_1258-68139.jpg');");
        
        populateCartItems(tilePane);
        scrollPane.setContent(tilePane);

        // Add ScrollPane to VBox
        getChildren().add(scrollPane);

        // Initialize total price label
        totalPriceLabel = new Label();
        totalPriceLabel.setFont(javafx.scene.text.Font.font("Arial", javafx.scene.text.FontWeight.BOLD, 20));
        updateTotalPrice();
        getChildren().add(totalPriceLabel);

        // Book Interior Designer button with creative effect
        Button bookDesignerButton = new Button("Book Interior Designer");
        bookDesignerButton.setFont(javafx.scene.text.Font.font("Arial", javafx.scene.text.FontWeight.BOLD, 20));
        bookDesignerButton.setPrefSize(200, 50);
        bookDesignerButton.setOnAction(event -> {
            // Show booking confirmation dialog with effect
            showBookingConfirmationDialogWithEffect();
        });
        getChildren().add(bookDesignerButton);
    }

    private void populateCartItems(TilePane tilePane) {
        for (CartItem item : cartItems) {
            Image image = new Image(item.getImageUrl());
            ImageView imageView = new ImageView(image);
            imageView.setFitWidth(400);
            imageView.setFitHeight(400);

            VBox itemBox = new VBox(10);
            itemBox.setAlignment(Pos.CENTER);
            itemBox.getChildren().addAll(imageView, new Label("Price: " + item.getPrice()), new Label("Type: " + item.getItemType()));

            // Optionally, add a button to remove item from cart
            Image removeImage = new Image(getClass().getResourceAsStream("/images/remove.png"));
            ImageView removeImageView = new ImageView(removeImage);
           removeImageView.setFitWidth(30);
            removeImageView.setFitHeight(30);
            Button removeButton = new Button("",removeImageView);
            removeButton.setOnAction(e -> {
                HomeRenovationApp.removeFromCart(item);
                tilePane.getChildren().remove(itemBox); // Remove from TilePane
                updateTotalPrice();
            });
            itemBox.getChildren().add(removeButton);

            tilePane.getChildren().add(itemBox); // Add item to TilePane
        }
    }

    private void updateTotalPrice() {
        double totalPrice = calculateTotalPrice();
        String formattedPrice = formatCurrency(totalPrice);
        totalPriceLabel.setText("Total Price: " + formattedPrice);
    }

    private double calculateTotalPrice() {
        double total = 0;
        for (CartItem item : cartItems) {
            try {
                total += parsePrice(item.getPrice());
            } catch (ParseException e) {
                System.err.println("Invalid price format: " + item.getPrice());
            }
        }
        return total;
    }

    private double parsePrice(String price) throws ParseException {
        // Remove currency symbol and commas
        String cleanedPrice = price.replaceAll("[^\\d.]", "");
        return NumberFormat.getNumberInstance(Locale.ENGLISH).parse(cleanedPrice).doubleValue();
    }

    private String formatCurrency(double amount) {
        NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
        return formatter.format(amount);
    }

    private void showBookingConfirmationDialogWithEffect() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Booking Confirmation");
        alert.setHeaderText(null);
        alert.setContentText("Designer is booked successfully");
    
        // Get the "Book Interior Designer" button
        Button bookDesignerButton = null;
        for (javafx.scene.Node node : getChildren()) {
            if (node instanceof Button && ((Button) node).getText().equals("Book Interior Designer")) {
                bookDesignerButton = (Button) node;
                break;
            }
        }
    
        if (bookDesignerButton != null) {
            final Button finalBookDesignerButton = bookDesignerButton; // Final or effectively final
            
            // Display message label with effect
            Label messageLabel = new Label("You Booked a Designer");
            messageLabel.setStyle("-fx-font-size: 40px; -fx-text-fill: #333333;");
            getChildren().add(messageLabel);
    
            // Add creative effect before showing the alert
            Thread effectThread = new Thread(() -> {
                try {
                    for (int i = 0; i < 3; i++) {
                        Thread.sleep(200); // Sleep for 200 milliseconds
                        finalBookDesignerButton.setDisable(true);
                        messageLabel.setVisible(false);
                        Thread.sleep(200);
                        finalBookDesignerButton.setDisable(false);
                        messageLabel.setVisible(true);
                    }
                    Thread.sleep(500); // Wait a bit longer after effect
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    // Remove message label after effect
                    getChildren().remove(messageLabel);
                    alert.showAndWait();
                }
            });
            effectThread.start();
        } else {
            System.err.println("Button 'Book Interior Designer' not found.");
        }
    }
}

    